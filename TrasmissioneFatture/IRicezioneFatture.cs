﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TrasmissioneFatture
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0", ConfigurationName = "RicezioneFatture")]
    public interface IRicezioneFatture
    {

        // CODEGEN: Generazione di un contratto di messaggio perché l'operazione RiceviFatture non è RPC né incapsulata da documenti.
        //[System.ServiceModel.OperationContractAttribute(Action = "http://www.fatturapa.it/RicezioneFatture/RiceviFattureSdI", ReplyAction = "*")]
        [OperationContract]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        RiceviFattureResponse RiceviFatture(RiceviFattureRequest request);

        //[System.ServiceModel.OperationContractAttribute(Action = "http://www.fatturapa.it/RicezioneFatture/RiceviFattureSdI", ReplyAction = "*")]
        //System.Threading.Tasks.Task<RiceviFattureResponse> RiceviFattureAsync(RiceviFattureRequest request);

        // CODEGEN: Generazione di un contratto di messaggio perché l'operazione NotificaDecorrenzaTermini non è RPC né incapsulata da documenti.
        [System.ServiceModel.OperationContractAttribute(IsOneWay = true, Action = "http://www.fatturapa.it/RicezioneFatture/NotificaDecorrenzaTermini")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        void NotificaDecorrenzaTermini(NotificaDecorrenzaTermini request);

        //[System.ServiceModel.OperationContractAttribute(IsOneWay = true, Action = "http://www.fatturapa.it/RicezioneFatture/NotificaDecorrenzaTermini")]
        //System.Threading.Tasks.Task NotificaDecorrenzaTerminiAsync(NotificaDecorrenzaTermini request);
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0/types")]
    public partial class fileSdIConMetadati_Type
    {

        private string identificativoSdIField;

        private string nomeFileField;

        private byte[] fileField;

        private string nomeFileMetadatiField;

        private byte[] metadatiField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer", Order = 0)]
        public string IdentificativoSdI
        {
            get
            {
                return this.identificativoSdIField;
            }
            set
            {
                this.identificativoSdIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string NomeFile
        {
            get
            {
                return this.nomeFileField;
            }
            set
            {
                this.nomeFileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "base64Binary", Order = 2)]
        public byte[] File
        {
            get
            {
                return this.fileField;
            }
            set
            {
                this.fileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public string NomeFileMetadati
        {
            get
            {
                return this.nomeFileMetadatiField;
            }
            set
            {
                this.nomeFileMetadatiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "base64Binary", Order = 4)]
        public byte[] Metadati
        {
            get
            {
                return this.metadatiField;
            }
            set
            {
                this.metadatiField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0/types")]
    public partial class rispostaRiceviFatture_Type
    {

        private esitoRicezione_Type esitoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public esitoRicezione_Type Esito
        {
            get
            {
                return this.esitoField;
            }
            set
            {
                this.esitoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0/types")]
    public enum esitoRicezione_Type
    {

        /// <remarks/>
        ER01,
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class RiceviFattureRequest
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0/types", Order = 0)]
        public fileSdIConMetadati_Type fileSdIConMetadati;

        public RiceviFattureRequest()
        {
        }

        public RiceviFattureRequest(fileSdIConMetadati_Type fileSdIConMetadati)
        {
            this.fileSdIConMetadati = fileSdIConMetadati;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class RiceviFattureResponse
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/ricezione/v1.0/types", Order = 0)]
        public rispostaRiceviFatture_Type rispostaRiceviFatture;

        public RiceviFattureResponse()
        {
        }

        public RiceviFattureResponse(rispostaRiceviFatture_Type rispostaRiceviFatture)
        {
            this.rispostaRiceviFatture = rispostaRiceviFatture;
        }
    }



    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface RicezioneFattureChannel : IRicezioneFatture, System.ServiceModel.IClientChannel
    {
    }


}
