﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrasmissioneFatture.Model
{
    public class ErroriSDI
    {
        public int id { get; set; }
        public string Cod { get; set; }
        public string Descrizione { get; set; }
    }
}