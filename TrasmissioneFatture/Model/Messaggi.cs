﻿namespace TrasmissioneFatture.Model
{
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "RicevutaConsegna", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class RicevutaConsegna_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("RicevutaConsegna_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "NotificaScarto", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class NotificaScarto_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("NotificaScarto_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "NotificaMancataConsegna", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class NotificaMancataConsegna_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("NotificaMancataConsegna_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "NotificaEsito", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class NotificaEsito_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("NotificaEsito_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "AttestazioneTrasmissioneFattura", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class AttestazioneTrasmissioneFattura_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("AttestazioneTrasmissioneFattura_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class RiferimentoArchivio_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("RiferimentoArchivio_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class ListaErrori_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("ListaErrori_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class Errore_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("Errore_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "MetadatiInvioFile", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class MetadatiInvioFile_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("MetadatiInvioFile_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "NotificaEsitoCommittente", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class NotificaEsitoCommittente_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("NotificaEsitoCommittente_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "ScartoEsitoCommittente", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class ScartoEsitoCommittente_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("ScartoEsitoCommittente_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "NotificaDecorrenzaTermini", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0", IsNullable = false)]
    public partial class NotificaDecorrenzaTermini_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("NotificaDecorrenzaTermini_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "EsitoCommittente_Type", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0")]
    public enum EsitoCommittente_Type : int
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        EC01 = 0,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        EC02 = 1,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "Scarto_Type", Namespace = "http://www.fatturapa.gov.it/sdi/messaggi/v1.0")]
    public enum Scarto_Type : int
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        EN00 = 0,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        EN01 = 1,
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class RiferimentoFattura_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("RiferimentoFattura_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class Destinatario_Type : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("Destinatario_Type", "http://www.fatturapa.gov.it/sdi/messaggi/v1.0");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }
}