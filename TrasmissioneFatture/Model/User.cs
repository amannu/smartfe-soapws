﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TrasmissioneFatture.Model
{
    public class User
    {
        public long id { get; set; }
        public DateTime CreateDate { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Denominazione { get; set; }
        public string IdFiscaleIVA { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public DateTime? LastAccessDate { get; set; }

        public User(long id)
        {
            using (SqlConnection cn = ConfigurationManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_UserById", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@id", id));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            if (sdr["id"] != DBNull.Value) this.id = (long)sdr["id"];
                            if (sdr["CreateDate"] != DBNull.Value) this.CreateDate = (DateTime)sdr["CreateDate"];
                            if (sdr["Username"] != DBNull.Value) this.Username = (string)sdr["Username"];
                            if (sdr["Password"] != DBNull.Value) this.Password = (string)sdr["Password"];
                            if (sdr["Name"] != DBNull.Value) this.Name = (string)sdr["Name"];
                            if (sdr["Surname"] != DBNull.Value) this.Surname = (string)sdr["Surname"];
                            if (sdr["Denominazione"] != DBNull.Value) this.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value) this.IdFiscaleIVA = (string)sdr["IdFiscaleIVA"];
                            if (sdr["Address"] != DBNull.Value) this.Address = (string)sdr["Address"];
                            if (sdr["PostCode"] != DBNull.Value) this.PostCode = (string)sdr["PostCode"];
                            if (sdr["City"] != DBNull.Value) this.City = (string)sdr["City"];
                            if (sdr["Country"] != DBNull.Value) this.Country = (string)sdr["Country"];
                            if (sdr["Email"] != DBNull.Value) this.Email = (string)sdr["Email"];
                            if (sdr["LastAccessDate"] != DBNull.Value) this.LastAccessDate = (DateTime)sdr["LastAccessDate"];
                        }
                    }
                }
                catch (Exception ex)
                {
                    //GlobalUtils.WriteException(ex);
                    //Mouse.OverrideCursor = null;
                    //MessageBox.Show(ex.Message, "Errore", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                }
            }
        }

        public User(string idFiscale)
        {
            using (SqlConnection cn = ConfigurationManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_UserByIdFiscale", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@idFiscaleIva", idFiscale));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            if (sdr["id"] != DBNull.Value) this.id = (long)sdr["id"];
                            if (sdr["CreateDate"] != DBNull.Value) this.CreateDate = (DateTime)sdr["CreateDate"];
                            if (sdr["Username"] != DBNull.Value) this.Username = (string)sdr["Username"];
                            if (sdr["Password"] != DBNull.Value) this.Password = (string)sdr["Password"];
                            if (sdr["Name"] != DBNull.Value) this.Name = (string)sdr["Name"];
                            if (sdr["Surname"] != DBNull.Value) this.Surname = (string)sdr["Surname"];
                            if (sdr["Denominazione"] != DBNull.Value) this.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value) this.IdFiscaleIVA = (string)sdr["IdFiscaleIVA"];
                            if (sdr["Address"] != DBNull.Value) this.Address = (string)sdr["Address"];
                            if (sdr["PostCode"] != DBNull.Value) this.PostCode = (string)sdr["PostCode"];
                            if (sdr["City"] != DBNull.Value) this.City = (string)sdr["City"];
                            if (sdr["Country"] != DBNull.Value) this.Country = (string)sdr["Country"];
                            if (sdr["Email"] != DBNull.Value) this.Email = (string)sdr["Email"];
                            if (sdr["LastAccessDate"] != DBNull.Value) this.LastAccessDate = (DateTime)sdr["LastAccessDate"];
                        }
                    }
                }
                catch (Exception ex)
                {
                    //GlobalUtils.WriteException(ex);
                    //Mouse.OverrideCursor = null;
                    //MessageBox.Show(ex.Message, "Errore", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                }
            }
        }
    }

}