﻿using System;
using System.Data.SqlClient;

namespace TrasmissioneFatture
{
    public static class ConfigurationManager
    {
        public static readonly bool DEBUG_MODE = true;

        //Configurazione locale Andrea
        //private static readonly string SQLSERVER_SERVER_NAME = @"UTENTE-PC\SQLEXPRESS";//@"IS-5B05226C\SQLEXPRESS";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_USR";//"sa";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";//"HfQd43:y0C";

        //Configurazione locale Giovanni
        //private static readonly string SQLSERVER_SERVER_NAME = @"GIOVANNI-ASUS\SQLEXPRESS";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_Commerciale";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";

        //Configurazione ThinkServer
        //private static readonly string SQLSERVER_SERVER_NAME = @"THINKSERVER";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_USR";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";

        //Configurazione Coretech
        private static readonly string SQLSERVER_SERVER_NAME = @"IS-5B05226C\SQLEXPRESS";
        private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        private static readonly string SQLSERVER_USERNAME = "sa";
        private static readonly string SQLSERVER_PASSWORD = "HfQd43:y0C";



        private static volatile string cnStr;

        /// <summary>
        /// Restituisce la stringa di connessione
        /// </summary>
        /// <returns></returns>
        public static string getConnectionString()
        {
            if (String.IsNullOrEmpty(cnStr))
            {
                //Per accesso tramite IP

                //da me se metto Network Library=DBMSSOCN non si connette
                //cnStr = @"Server=%1;Network Library=DBMSSOCN;Initial Catalog=%2;User Id=%3;Password=%4;";
                cnStr = @"Server=%1;Initial Catalog=%2;User Id=%3;Password=%4;";
                //Per accesso tramite DOMAIN_NAME
                //string cnStr = @"Server=%1;Initial Catalog=%2;User Id=%3;Password=%4;";
                cnStr = cnStr.Replace("%1", SQLSERVER_SERVER_NAME).Replace("%2", SQLSERVER_DB_NAME)
                    .Replace("%3", SQLSERVER_USERNAME).Replace("%4", SQLSERVER_PASSWORD);

                //cnStr = "Server=GIOVANNI-ASUS\\SQLExpress;Initial Catalog=dbBrandaniLeaflet;User Id=Brandani_USR;Password=sgba137375;";
                //cnStr = "Server=THINKSERVER;Initial Catalog=dbSmartFE;User Id=ERepair_USR;Password=sgba137375;";
                //cnStr = "Server=5SPACESV1\\SQLEXPRESS;Initial Catalog=dbBrandaniLeaflet;User Id=Brandani_USR;Password=sgb@137375;";
            }
            return cnStr;
        }

        /// <summary>
        /// Ritorna la variabile contenente le informazioni di configurazione
        /// Accesso concorrente permesso
        /// </summary>
        /// <returns></returns>
        public static SqlConnection GetDbConnection()
        {
            //if (cn == null)
            //    cn = newConnection();

            //if (cn.State == System.Data.ConnectionState.Closed)
            //{
            //    cn.ConnectionString = getConnectionString();
            //    return cn;
            //}
            //else


            return newConnection();
        }

        /// <summary>
        /// Crea e restituisce una nuova connessione con il db
        /// </summary>
        /// <returns></returns>
        private static SqlConnection newConnection()
        {
            SqlConnection cn = new SqlConnection(getConnectionString());
            cn.Open();
            //Ottimizza le prestazioni
            //Vedere http://www.sql-speed.com/2009/12/why-does-sql-server-management-studio_07.html
            //e http://stackoverflow.com/questions/2736638/sql-query-slow-in-net-application-but-instantaneous-in-sql-server-management-st
            using (SqlCommand comm = new SqlCommand("SET ARITHABORT ON", cn))
            {
                comm.ExecuteNonQuery();
            }
            cn.Close();
            return cn;
        }

    }
}
