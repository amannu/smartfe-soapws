﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.IO.Compression;
using System.Xml.Serialization;
using RestWS.Models;
using TrasmissioneFatture.Model;

namespace TrasmissioneFatture
{
    public class MyUtils
    {
        public static string getXmlElementValue(byte[] bytes, string element)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string xml = Encoding.UTF8.GetString(bytes);
                doc.LoadXml(xml);
                return doc.GetElementsByTagName(element)[0].InnerText;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<ErroriSDI> getXmlErroriSDI(string lista)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<ErroriSDI>));
                using (TextReader reader = new StringReader(lista))
                {
                   return (List<ErroriSDI>)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static RiferimentoFattura_Type getXmlRiferimentoFatturaSDI(string lista)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(RiferimentoFattura_Type));
                using (TextReader reader = new StringReader(lista))
                {
                    return (RiferimentoFattura_Type)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool ByteArrayToZipFile(string fileName, byte[] byteArray)
        {
            try
            {

                using (ZipArchive zip = ZipFile.Open(fileName + ".zip", ZipArchiveMode.Create))
                {
                    using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(byteArray, 0, byteArray.Length);                        
                    }
                    zip.CreateEntryFromFile(fileName, Path.GetFileName(fileName));
                    File.Delete(fileName);
                    return true;
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        public static void AggiornaStatoFatture(List<FatturaModel> fatture, int ricevutaConsegnaState)
        {
            throw new NotImplementedException();
        }
    }
}