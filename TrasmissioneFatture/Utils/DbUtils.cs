﻿using FatturaElettronica;
using FatturaElettronica.Impostazioni;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using RestWS.Models;
using RestWS.Utils;
using TrasmissioneFatture.Model;

namespace TrasmissioneFatture
{
    public class DbUtils
    {

        public static string GetConfigField(string field)
        {
            SqlConnection con = null;
            SqlDataReader rdr = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    const string cmdString =
                    "SELECT value FROM tbConfig WHERE id = @field";
                    var cmd = new SqlCommand(cmdString, con);
                    cmd.Parameters.Add(new SqlParameter("@field", field));
                    rdr = cmd.ExecuteReader();
                    if (rdr == null)
                    {
                        return null;
                    }
                    else if (rdr.Read())
                    {
                        return Convert.ToString(rdr[0]);

                    }

                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
            }
            finally
            {
                if (con != null) con.Close();
            }
            return null;
        }

        public static string GetValue(long id, string table, string descrCol = "Descrizione")
        {
            SqlConnection con = null;
            //SqlDataReader rdr = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_Value", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    cmd.Parameters.Add(new SqlParameter("@Table", table));
                    cmd.Parameters.Add(new SqlParameter("@DescrCol", descrCol));
                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.Read())
                    {
                        return Convert.ToString(sdr[descrCol]);
                    }
                    return null;
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static List<FatturaModel> getFattureDaIdSdi(long identificativoSdI)
        {
            SqlConnection con = null;
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("proc_sel_FattureAttiveIdSDI", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSdI", identificativoSdI));
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        FatturaModel fattura = new FatturaModel();
                        if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                        if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                        if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                        if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                        if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                        if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                        if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                        if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                        if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                        if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                        if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                        if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                        if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                        if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                        if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                        if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                        if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        //if (sdr["StatoMittBreve"] != DBNull.Value) fattura.StatoMittBreve = (string)sdr["StatoMittBreve"];
                        //if (sdr["StatoMittLungo"] != DBNull.Value) fattura.StatoMittLungo = (string)sdr["StatoMittLungo"];
                        if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                        fatture.Add(fattura);
                    }
                    return fatture;
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                return fatture;
            }
            finally
            {
                if (con != null) con.Close();
            }


        }

        public static void RicevutaConsegna(long identificativoSdI, List<FatturaModel> fatture, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.RICEVUTA_CONSEGNA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@Titolo", Const.RICEVUTA_CONSEGNA_TITLE));
                    cmd.Parameters.Add(new SqlParameter("@Descrizione", Const.RICEVUTA_CONSEGNA_DESCRIPTION));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.RICEVUTA_CONSEGNA_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }

        }

        public static void NotificaMancataConsegna(long identificativoSdI, List<FatturaModel> fatture, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDIAtt", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_MANCATA_CONSEGNA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NOTIFICA_MANCATA_CONSEGNA_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }

        }

        public static void NotificaScarto(long identificativoSdI, List<FatturaModel> fatture, List<ErroriSDI> errori, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDIAtt", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_SCARTO_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    foreach (ErroriSDI errore in errori)
                    {
                        cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", identificativoSdI));
                        cmd.Parameters.Add(new SqlParameter("@idErrore", errore.id));
                        //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static void NotificaEsito(long identificativoSdI, string esito, List<FatturaModel> fatture, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDIAtt", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_MANCATA_CONSEGNA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@Descrizione", descrizione));
                    //cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NOTIFICA_MANCATA_CONSEGNA_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static void NotificaDecorrenzaTermini(long identificativoSdI, List<FatturaModel> fatture, List<ErroriSDI> errori, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDIAtt", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_DECORRENZA_TERMINI_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    if (fatture != null)
                        foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NOTIFICA_DECORRENZA_TERMINI_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    if (errori != null)
                    foreach (ErroriSDI errore in errori)
                    {
                        cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", identificativoSdI));
                        cmd.Parameters.Add(new SqlParameter("@idErrore", errore.id));
                        //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static void AttestazioneTrasmissioneFattura(long identificativoSdI, List<FatturaModel> fatture, List<ErroriSDI> errori, string nomeFile, string descrizione = null, string note = null)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDIAtt", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_ATTESTAZIONE_TRASMISSIONE_FATTURA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NOTIFICA_ATTESTAZIONE_TRASMISSIONE_FATTURA_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    foreach (ErroriSDI errore in errori)
                    {
                        cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", identificativoSdI));
                        cmd.Parameters.Add(new SqlParameter("@idErrore", errore.id));
                        //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static void InserisciFattura(Fattura fattura, long idUser)
        {
            SqlConnection con = null;
            SqlTransaction myTran = null;
            try
            {
                using (con = ConfigurationManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();

                    foreach (var body in fattura.Body)
                    {
                        var numeroFat = body.DatiGenerali.DatiGeneraliDocumento.Numero;
                        var dataFat = body.DatiGenerali.DatiGeneraliDocumento.Data;
                        SqlCommand command = new SqlCommand("proc_sel_FattureAttive", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = myTran;
                        command.Parameters.AddWithValue("NumeroFattura", numeroFat);
                        command.Parameters.AddWithValue("DataFattura", dataFat);
                        command.Parameters.AddWithValue("idUser", idUser);
                        using (SqlDataReader rdr = command.ExecuteReader())
                        {
                            if (rdr != null && rdr.Read())
                            {
                                //TODO la fattura è già stata caricata, segnare l'errore
                            }
                            else
                            {
                                command = new SqlCommand("proc_ins_FatturaAttiva", con);
                                command.CommandType = CommandType.StoredProcedure;
                                command.Transaction = myTran;
                                command.Parameters.AddWithValue("idUser", idUser);
                                command.Parameters.AddWithValue("NomeFile", "");
                                command.Parameters.AddWithValue("ProgressivoInvio",
                                                                fattura.Header.DatiTrasmissione.ProgressivoInvio);
                                command.Parameters.AddWithValue("DenominazioneCessionario",
                                                                fattura.Header.CessionarioCommittente.DatiAnagrafici
                                                                       .Anagrafica.Denominazione);
                                command.Parameters.AddWithValue("IdFiscaleIVACessionario",
                                                                fattura.Header.CessionarioCommittente.DatiAnagrafici
                                                                       .IdFiscaleIVA.IdCodice +
                                                                fattura.Header.CessionarioCommittente.DatiAnagrafici
                                                                       .IdFiscaleIVA.IdPaese);
                                command.Parameters.AddWithValue("TipoDocumento",
                                                                body.DatiGenerali.DatiGeneraliDocumento.TipoDocumento);
                                command.Parameters.AddWithValue("NumeroFattura",
                                                                body.DatiGenerali.DatiGeneraliDocumento.Numero);
                                command.Parameters.AddWithValue("Data", body.DatiGenerali.DatiGeneraliDocumento.Data);
                                command.Parameters.AddWithValue("ImportoTotaleDocumento",
                                                                body.DatiPagamento[0].DettaglioPagamento[0]
                                                                    .ImportoPagamento);
                                command.Parameters.AddWithValue("idStato", 1);
                                command.Parameters.AddWithValue("DataCreate", DateTime.Now);
                                //command.Parameters.AddWithValue("IpCreate", GlobalUtils.GetIPv4Address());
                                command.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;
                                command.ExecuteNonQuery();
                                long idFat = Convert.ToInt64(command.Parameters["@retValue"].Value);

                                foreach (var riga in body.DatiBeniServizi.DettaglioLinee)
                                {
                                    command = new SqlCommand("proc_ins_FatturaRigAttiva", con);
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.Transaction = myTran;
                                    command.Parameters.AddWithValue("idFatt", idFat);
                                    command.Parameters.AddWithValue("Descrizione", riga.Descrizione);
                                    command.Parameters.AddWithValue("Quantita", riga.Quantita);
                                    command.Parameters.AddWithValue("Importo", riga.PrezzoTotale);
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    myTran.Commit();
                }
            }
            catch (Exception e)
            {
                myTran.Rollback();
                //TODO gestire l'errore
            }
        }
    }
  
            
    }




