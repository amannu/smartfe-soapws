﻿using System.ComponentModel;

namespace TrasmissioneFatture
{
    // NOTA: è possibile utilizzare il comando "Rinomina" del menu "Refactoring" per modificare il nome di classe "RicezioneFatture" nel codice, nel file svc e nel file di configurazione contemporaneamente.
    // NOTA: per avviare il client di prova WCF per testare il servizio, selezionare RicezioneFatture.svc o RicezioneFatture.svc.cs in Esplora soluzioni e avviare il debug.
    public class RicezioneFatture : IRicezioneFatture
    {
        public RiceviFattureResponse RiceviFatture(RiceviFattureRequest request)
        {
            string s = request.fileSdIConMetadati.ToString();
            RiceviFattureResponse response = new RiceviFattureResponse();
            response.rispostaRiceviFatture = new rispostaRiceviFatture_Type();
            response.rispostaRiceviFatture.Esito = esitoRicezione_Type.ER01;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += BwOnDoWork;
            bw.RunWorkerCompleted += BwOnRunWorkerCompleted;
            bw.RunWorkerAsync(request.fileSdIConMetadati);
            
            

            return response;
        }

        private void BwOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            //throw new NotImplementedException();
        }

        private void BwOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            fileSdIConMetadati_Type request = (fileSdIConMetadati_Type)doWorkEventArgs.Argument;
            //todo controlla che il cliente sia nostro
            //todo controlla il tipo di file se zip o xml
            //todo memorizza il file 
            //todo chiama dbUtils.InserisciFattura
        }

        public void NotificaDecorrenzaTermini(NotificaDecorrenzaTermini request)
        {
            
        }

    }
}
