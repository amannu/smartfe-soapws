﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Xml;
using RestWS.Models;
using RestWS.Utils;
using TrasmissioneFatture.Model;

namespace TrasmissioneFatture
{
    // NOTA: è possibile utilizzare il comando "Rinomina" del menu "Refactoring" per modificare il nome di classe "Service1" nel codice, nel file svc e nel file di configurazione contemporaneamente.
    // NOTA: per avviare il client di prova WCF per testare il servizio, selezionare Service1.svc o Service1.svc.cs in Esplora soluzioni e avviare il debug.

     [ConsoleServiceBehavior]
     [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [XmlSerializerFormat()]
    public class TrasmissioneFatture : ITrasmissioneFatture
    {

       

        public void RicevutaConsegna(RicevutaConsegna request)
        {
            //parsing xml
            string note = MyUtils.getXmlElementValue(request.ricevutaConsegna1.File, "Note");

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.ricevutaConsegna1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.ricevutaConsegna1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.ricevutaConsegna1.File);

                //cambio stato
                DbUtils.RicevutaConsegna(Convert.ToInt64(request.ricevutaConsegna1.IdentificativoSdI), fatture,
                                         filePath + ".zip");
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }

        }


        public void NotificaMancataConsegna(NotificaMancataConsegna request)
        {
            //parsing xml
            string note = MyUtils.getXmlElementValue(request.notificaMancataConsegna1.File, "Note");

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.notificaMancataConsegna1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.notificaMancataConsegna1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.notificaMancataConsegna1.File);

                //cambio stato
                DbUtils.NotificaMancataConsegna(Convert.ToInt64(request.notificaMancataConsegna1.IdentificativoSdI), fatture, filePath + ".zip");
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }
        }


        public void NotificaScarto(NotificaScarto request)
        {
            //parsing xml
            string note = MyUtils.getXmlElementValue(request.notificaScarto1.File, "Note");
            List<ErroriSDI> errori =
                MyUtils.getXmlErroriSDI(MyUtils.getXmlElementValue(request.notificaScarto1.File, "ListaErrori"));

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.notificaScarto1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.notificaScarto1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.notificaScarto1.File);

                //cambio stato
                DbUtils.NotificaScarto(Convert.ToInt64(request.notificaScarto1.IdentificativoSdI), fatture, errori, filePath + ".zip", null, note);
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }
        }


        public void NotificaEsito(NotificaEsito request)
        {
            //parsing xml
            string descrizione = MyUtils.getXmlElementValue(request.notificaEsito1.File, "Descrizione");
            string esito = MyUtils.getXmlElementValue(request.notificaEsito1.File, "Esito");

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.notificaEsito1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.notificaEsito1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.notificaEsito1.File);

                //cambio stato
                DbUtils.NotificaEsito(Convert.ToInt64(request.notificaEsito1.IdentificativoSdI), esito, fatture, filePath + ".zip", descrizione);
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }
        }


        public void NotificaDecorrenzaTermini(NotificaDecorrenzaTermini request)
        {
            //parsing xml
            string descrizione = MyUtils.getXmlElementValue(request.notificaDecorrenzaTermini1.File, "Descrizione");
            string note = MyUtils.getXmlElementValue(request.notificaDecorrenzaTermini1.File, "Note");
            RiferimentoFattura_Type riferimentoFattura =
                MyUtils.getXmlRiferimentoFatturaSDI(MyUtils.getXmlElementValue(request.notificaDecorrenzaTermini1.File,
                                                                               "RiferimentoFattura"));

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.notificaDecorrenzaTermini1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.notificaDecorrenzaTermini1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.notificaDecorrenzaTermini1.File);

                //cambio stato
                DbUtils.NotificaDecorrenzaTermini(Convert.ToInt64(request.notificaDecorrenzaTermini1.IdentificativoSdI), fatture, null, filePath + ".zip", descrizione, note);
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }
        }


        public void AttestazioneTrasmissioneFattura(AttestazioneTrasmissioneFattura request)
        {
            //parsing xml
            string descrizione = MyUtils.getXmlElementValue(request.attestazioneTrasmissioneFattura1.File, "Descrizione");
            string note = MyUtils.getXmlElementValue(request.attestazioneTrasmissioneFattura1.File, "Note");

            //cercare fatture che hanno quell'identificativo sdi
            List<FatturaModel> fatture = DbUtils.getFattureDaIdSdi(Convert.ToInt64(request.attestazioneTrasmissioneFattura1.IdentificativoSdI));

            if (fatture.Count > 0)
            {
                string basepath = DbUtils.GetConfigField("FILES_PATH") + Path.DirectorySeparatorChar + fatture[0].IdUser +
                                  Path.DirectorySeparatorChar + "MessaggiSDI";

                if (!Directory.Exists(basepath))
                    Directory.CreateDirectory(basepath);
                string filePath = basepath + Path.DirectorySeparatorChar + request.attestazioneTrasmissioneFattura1.NomeFile;
                MyUtils.ByteArrayToZipFile(filePath, request.attestazioneTrasmissioneFattura1.File);

                //cambio stato
                DbUtils.AttestazioneTrasmissioneFattura(Convert.ToInt64(request.attestazioneTrasmissioneFattura1.IdentificativoSdI), fatture, null, filePath + ".zip", descrizione, note);
            }
            else
            {
                //todo devo generare un errore interno da loggare in qualche modo, mi è arrivata una notifica errata da sdi
            }
        }

    }

    public class ConsoleServiceBehavior : Attribute, IServiceBehavior
    {
        

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
                                         BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (var item in serviceHostBase.Description.Endpoints)
            {
                item.Behaviors.Add(new ConsoleEndpointBehavior());
            }
        }
        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
        }
    }
    public class ConsoleMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        public Message CreateMessage(Message message)
        {
            if (message != null)
            {
                MessageBuffer buffer = message.CreateBufferedCopy(Int32.MaxValue);
                Message msg = buffer.CreateMessage();
                StringBuilder sb = new StringBuilder();
                using (XmlWriter xw = XmlWriter.Create(sb))
                {
                    msg.WriteMessage(xw);
                    xw.Close();
                }
                return buffer.CreateMessage();
            }
            return message;
        }
        private Message TransformMessage(Message oldMessage)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                XmlWriter xw = XmlWriter.Create(ms);
                oldMessage.WriteMessage(xw);
                xw.Flush();
                string body = Encoding.UTF8.GetString(ms.ToArray());
                body = body.Replace("<ns2:", "<");
                body = body.Replace("</ns2:", "</");
                body = body.Replace("xmlns:ns2=", "xmlns=");
                xw.Close();
                ms = new MemoryStream(Encoding.UTF8.GetBytes(body));
                XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas());
                Message newMessage = Message.CreateMessage(xdr, int.MaxValue, oldMessage.Version);
                newMessage.Properties.CopyProperties(oldMessage.Properties);
                return newMessage;
            }
            catch
            {
                return oldMessage;
            }
        }
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            request = TransformMessage(request);
#if DEBUG
            LogWriter.WriteLog("Trasmissione Fatture AfterReceiveRequest", "tflog", new object[] { request });
#endif
            return request;
        }
        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            reply = CreateMessage(reply);
#if DEBUG
            LogWriter.WriteLog("Trasmissione Fatture BeforeSendReply", "tflog", new object[] { reply });
#endif
        }
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            reply = CreateMessage(reply);
#if DEBUG
            LogWriter.WriteLog("Trasmissione Fatture AfterReceiveReply", "tflog", new object[] { reply });
#endif
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            request = CreateMessage(request);
#if DEBUG
            LogWriter.WriteLog("Trasmissione Fatture BeforeSendRequest", "tflog", new object[] { request });
#endif
            return null;
        }
    }

    public class ConsoleEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }
        public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new ConsoleMessageInspector());
        }
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new ConsoleMessageInspector());
        }
        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }


}
