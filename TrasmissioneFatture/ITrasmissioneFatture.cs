﻿using System.ServiceModel;

namespace TrasmissioneFatture
{
    // NOTA: è possibile utilizzare il comando "Rinomina" del menu "Refactoring" per modificare il nome di interfaccia "IService1" nel codice e nel file di configurazione contemporaneamente.
    [ServiceContract]
    public interface ITrasmissioneFatture
    {
        [System.ServiceModel.OperationContractAttribute(IsOneWay = true, Action = "http://www.fatturapa.it/TrasmissioneFatture/RicevutaConsegna")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        void RicevutaConsegna(RicevutaConsegna request);

    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://www.fatturapa.it/TrasmissioneFatture/NotificaMancataConsegna")]
    [System.ServiceModel.XmlSerializerFormatAttribute()]
    void NotificaMancataConsegna(NotificaMancataConsegna request);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://www.fatturapa.it/TrasmissioneFatture/NotificaScarto")]
    [System.ServiceModel.XmlSerializerFormatAttribute()]
    void NotificaScarto(NotificaScarto request);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://www.fatturapa.it/TrasmissioneFatture/NotificaEsito")]
    [System.ServiceModel.XmlSerializerFormatAttribute()]
    void NotificaEsito(NotificaEsito request);
   
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://www.fatturapa.it/TrasmissioneFatture/NotificaDecorrenzaTermini")]
    [System.ServiceModel.XmlSerializerFormatAttribute()]
    void NotificaDecorrenzaTermini(NotificaDecorrenzaTermini request);
        
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://www.fatturapa.it/TrasmissioneFatture/AttestazioneTrasmissioneFattura")]
    [System.ServiceModel.XmlSerializerFormatAttribute()]
    void AttestazioneTrasmissioneFattura(AttestazioneTrasmissioneFattura request);

    }



    [System.CodeDom.Compiler.GeneratedCodeAttribute("svcutil", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types")]
    public class fileSdI_Type
    {

        private string identificativoSdIField;

        private string nomeFileField;

        private byte[] fileField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer", Order = 0)]
        public string IdentificativoSdI
        {
            get
            {
                return this.identificativoSdIField;
            }
            set
            {
                this.identificativoSdIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
        public string NomeFile
        {
            get
            {
                return this.nomeFileField;
            }
            set
            {
                this.nomeFileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "base64Binary", Order = 2)]
        public byte[] File
        {
            get
            {
                return this.fileField;
            }
            set
            {
                this.fileField = value;
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class RicevutaConsegna
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "ricevutaConsegna", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type ricevutaConsegna1;

        public RicevutaConsegna()
        {
        }

        public RicevutaConsegna(fileSdI_Type ricevutaConsegna1)
        {
            this.ricevutaConsegna1 = ricevutaConsegna1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class NotificaMancataConsegna
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "notificaMancataConsegna", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type notificaMancataConsegna1;

        public NotificaMancataConsegna()
        {
        }

        public NotificaMancataConsegna(fileSdI_Type notificaMancataConsegna1)
        {
            this.notificaMancataConsegna1 = notificaMancataConsegna1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class NotificaScarto
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "notificaScarto", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type notificaScarto1;

        public NotificaScarto()
        {
        }

        public NotificaScarto(fileSdI_Type notificaScarto1)
        {
            this.notificaScarto1 = notificaScarto1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class NotificaEsito
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "notificaEsito", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type notificaEsito1;

        public NotificaEsito()
        {
        }

        public NotificaEsito(fileSdI_Type notificaEsito1)
        {
            this.notificaEsito1 = notificaEsito1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class NotificaDecorrenzaTermini
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "notificaDecorrenzaTermini", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type notificaDecorrenzaTermini1;

        public NotificaDecorrenzaTermini()
        {
        }

        public NotificaDecorrenzaTermini(fileSdI_Type notificaDecorrenzaTermini1)
        {
            this.notificaDecorrenzaTermini1 = notificaDecorrenzaTermini1;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class AttestazioneTrasmissioneFattura
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Name = "attestazioneTrasmissioneFattura", Namespace = "http://www.fatturapa.gov.it/sdi/ws/trasmissione/v1.0/types", Order = 0)]
        public fileSdI_Type attestazioneTrasmissioneFattura1;

        public AttestazioneTrasmissioneFattura()
        {
        }

        public AttestazioneTrasmissioneFattura(fileSdI_Type attestazioneTrasmissioneFattura1)
        {
            this.attestazioneTrasmissioneFattura1 = attestazioneTrasmissioneFattura1;
        }
    }



}
